class Leonardo < Formula
    desc "A universal theme artist"
    homepage "https://pisolutions.gitlab.io/eu.pisolutions.leonardo/"
    url "https://dl.bintray.com/pisolutions/pisolutions/eu/pisolutions/eu.pisolutions.leonardo.dist/0.1.0/eu.pisolutions.leonardo.dist-0.1.0.tar.gz"
    sha256 "7ebfdf0a7bd2e2813d258ed8e5dfb27f8a53dd20c5d27430de5b7c101e3cff1e"
    head "https://gitlab.com/pisolutions/eu.pisolutions.leonardo.git"

    bottle :unneeded

    depends_on :java => "1.8+"

    def install
        libexec.install Dir["*"]
        bin.install_symlink Dir["#{libexec}/bin/leonardo"]
    end

    test do
        system "#{bin}/leonardo", "--version"
    end
end
