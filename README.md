# Pi Solutions' Homebrew tap

Welcome to Pi Solutions' [Homebrew](https://brew.sh/) tap!

## Installation

``` shell
brew tap pisolutions/pisolutions https://gitlab.com/pisolutions/homebrew-pisolutions
```
